---
title: "Big Kahuna Burguer"
date: 2021-10-14T18:43:33-03:00
layout: "single"
type: "single"
draft: false
---
Prepare-se para conhecer uma hamburgueria extremamente famosa em São Paulo e ela conseguiu esse reconhecimento com um peso pesado, literalmente.

O lanche Ezequiel consta com "pequenos" cinco hamburguers de 140 gramas cada, 10 fatias de bacon, queijo Cheddar e prato, Crispy Onions, picles, cebolas roxas e uns outros alimentos verdes para parecer saudável. Como se não bastasse ele vem acompanhado de uma camisa do lanche caso você consiga vencer o Ezequiel e, assim, levar essa recordação para casa (tudo bem que de um jeito ou de outro, duvido alguém esquecer dele).

![Kahuna](../bigKahuna.jpeg#BK)