---
title: "Koburguer"
date: 2021-10-14T18:43:23-03:00
layout: "single"
type: "single"
draft: false
---

Se você é aquele tipo de pessoa que gosta de uns confetes e de coisas chiquetosas esse é o lugar. O Konurguer consta com burguers 100% feitos com carnes Wagyu certificadas (é uma carne especial para os leigos).

Umas das obras primas desse hamburgueria é o Spicy Baconpiry, que além da carne Wagyu, consta com um anel de bacon mergulhado em molho sweet spicy do Chef! Fora outras peculiaridades.

Outra recomendação é pedir a Cheddar bacon fries, que são 200g de batata com tudo de não saudável que a gente merece.

Por fim, se vossa pessoa que está lendo curte estar com pessoas, esse com certeza é o lugar. O Koburguer está localizado em um centro gastrônomico de Pinheiros que permite aquele rolê com os amiguinhos e a diversão é garantida!

![Koburguer](../Koburguer.jpeg#KB)