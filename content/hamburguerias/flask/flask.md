---
title: "SampaBurguer"
date: 2021-10-14T18:43:33-03:00
layout: "single"
type: "single"
draft: false
---
SampaBurguer com certeza está entre as melhores Hamburguerias da cidade de São Paulo. Com uma temática paulistana, o SampaBuruer honra esse título com um pouco de tudo que tem direito.

Um dos seus lanches de excelência se chama "Paraíso" que além do delicioso burguer vem acompanhado de um copo de chedar caprichado, maionese da casa e batatas rústicas salteadas com bacon e um tempero especial.

Caso você ainda não esteja satisfeito com isso para entupir as suas veias, o cliente ainda pode pedir uma jarra de Milkshake de 2 litros acompanhado de uma porção de chocolate em calda e ovolmaltine para você montar o seu shake do jeito que quiser!

![Hamburguer](../SampaBurguer.jpeg#A)

